package views;

import common.ConfigFile;
import common.Lane;
import common.Util;
import models.Bowler;
import models.LaneEvent;
import models.Party;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

/*
 *  constructs a prototype Lane View
 *
 */
public class LaneView implements Observer {

    JFrame frame;
    Container cpanel;
    Vector<Bowler> bowlers;
    //int cur;
    //Iterator bowlIt;
    JPanel[][] balls;
    JLabel[][] ballLabel;
    JPanel[][] scores;
    JLabel[][] scoreLabel;
    JPanel[][] ballGrid;
    JPanel[] pins;
    JLabel[] emoticons;
    JButton maintenance;
    Lane lane;
    int numFrames;
    int num23;
    //    private int roll;
    private boolean initDone;// = true;

    Image scaledImg, scaledImg2, scaledImg3, scaledImg4;

    public LaneView(Lane lane, int laneNum) {

        this.lane = lane;
        this.numFrames = lane.numberOfFrames;
        initDone = true;
        this.num23 = 2*numFrames+3;
        frame = new JFrame("Lane " + laneNum + ":");
        cpanel = frame.getContentPane();
        cpanel.setLayout(new BorderLayout());

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.setVisible(false);
            }
        });

        cpanel.add(new JPanel());

        ImageIcon emoticonImageIcon2 = new ImageIcon("views/em_envy.jpg");
        Image img2 = emoticonImageIcon2.getImage();
        ImageScaler imgScalar2 = new ImageScaler();
        scaledImg2 = imgScalar2.getScaledImage(img2, 100, 100 );
        ImageIcon emoticonImageIcon = new ImageIcon("views/em_happy.png");
        Image img = emoticonImageIcon.getImage();
        ImageScaler imgScalar = new ImageScaler();
        scaledImg = imgScalar.getScaledImage(img, 100, 100 );
        ImageIcon emoticonImageIcon3 = new ImageIcon("views/em_embarrased.jpg");
        Image img3 = emoticonImageIcon3.getImage();
        ImageScaler imgScalar3 = new ImageScaler();
        scaledImg3 = imgScalar3.getScaledImage(img3, 100, 100 );
        ImageIcon emoticonImageIcon4 = new ImageIcon("views/em_normal.png");
        Image img4 = emoticonImageIcon4.getImage();
        ImageScaler imgScalar4 = new ImageScaler();
        scaledImg4 = imgScalar4.getScaledImage(img4, 100, 100 );
    }

    public JFrame getFrame() {
        return frame;
    }

    private JPanel makeFrame(Party party) {

        initDone = false;
        bowlers = party.getMembers();
        int numBowlers = bowlers.size();

        JPanel panel = new JPanel();

        panel.setLayout(new GridLayout(0, 1));

        balls = new JPanel[numBowlers][num23];
        ballLabel = new JLabel[numBowlers][num23];
        scores = new JPanel[numBowlers][numFrames+1];
        scoreLabel = new JLabel[numBowlers][numFrames+1];
        ballGrid = new JPanel[numBowlers][numFrames+1];
        pins = new JPanel[numBowlers];
        emoticons = new JLabel[numBowlers];

        this.getClass().getResource("/views/em_normal.png");
        for (int i = 0; i != numBowlers; i++) {
            for (int j = 0; j != num23; j++) {
                ballLabel[i][j] = new JLabel(" ");
                balls[i][j] = new JPanel();
                balls[i][j].setBorder(
                        BorderFactory.createLineBorder(Color.BLACK));
                balls[i][j].add(ballLabel[i][j]);
            }
            ImageIcon emoticonImageIcon = new ImageIcon("views/em_normal.png");
            Image img = emoticonImageIcon.getImage();
            ImageScaler imgScalar = new ImageScaler();
            Image scaledImg = imgScalar.getScaledImage(img, 100, 100 );
            emoticons[i] = new JLabel(new ImageIcon(scaledImg));
        }

        for (int i = 0; i != numBowlers; i++) {
            for (int j = 0; j != numFrames; j++) {
                ballGrid[i][j] = new JPanel();
                ballGrid[i][j].setLayout(new GridLayout(0, 3));
                ballGrid[i][j].add(new JLabel("  "), BorderLayout.EAST);
                ballGrid[i][j].add(balls[i][2 * j], BorderLayout.EAST);
                ballGrid[i][j].add(balls[i][2 * j + 1], BorderLayout.EAST);
            }
            int j = numFrames;
            ballGrid[i][j] = new JPanel();
            if(j==9){
                ballGrid[i][j].setLayout(new GridLayout(0, 3));
                ballGrid[i][j].add(balls[i][2 * j]);
                ballGrid[i][j].add(balls[i][2 * j + 1]);
                ballGrid[i][j].add(balls[i][2 * j + 2]);
            }else{
                ballGrid[i][j].setLayout(new GridLayout(0, 3));
                ballGrid[i][j].add(new JLabel("  "), BorderLayout.EAST);
                ballGrid[i][j].add(balls[i][2 * j]);
                ballGrid[i][j].add(balls[i][2 * j + 1]);
                //ballGrid[i][j].add(balls[i][2 * j + 2]);
            }

        }

        for (int i = 0; i != numBowlers; i++) {
            pins[i] = new JPanel();
            pins[i].setBorder(BorderFactory.createTitledBorder(bowlers.get(i).getNickName()));
            pins[i].setLayout(new GridLayout(0, numFrames+2));
            pins[i].add(emoticons[i] , BorderLayout.EAST);
            for (int k = 0; k != numFrames+1; k++) {
                scores[i][k] = new JPanel();
                scoreLabel[i][k] = new JLabel("  ", SwingConstants.CENTER);
                scores[i][k].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                scores[i][k].setLayout(new GridLayout(0, 1));
                scores[i][k].add(ballGrid[i][k], BorderLayout.EAST);
                scores[i][k].add(scoreLabel[i][k], BorderLayout.SOUTH);
                pins[i].add(scores[i][k], BorderLayout.EAST);
            }
            panel.add(pins[i]);
        }

        initDone = true;
        return panel;
    }

    @Override
    public void update(Observable lane, Object le) {
        if (((LaneEvent) le).isPartyAssigned) {
            while (!initDone) {
                //System.out.println("chillin' here.");
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    System.err.println("LaneView Error occurred " + e);
                }
            }

            if (((LaneEvent) le).getFrameInfo().getFrameNum() == 1
                    && ((LaneEvent) le).getFrameInfo().getBall() == 0
                    && ((LaneEvent) le).getFrameInfo().getIndex() == 0) {
                System.out.println("Making the frame.");
                cpanel.removeAll();
                cpanel.add(makeFrame(((LaneEvent) le).getParty()), "Center");

                // Button Panel
                JPanel buttonPanel = new JPanel();
                buttonPanel.setLayout(new FlowLayout());

                maintenance = Util.createButton("Maintenance Call");
                maintenance.addActionListener(e -> LaneView.this.lane.pauseGame());
                JPanel maintenancePanel = new JPanel();
                maintenancePanel.setLayout(new FlowLayout());
                maintenancePanel.add(maintenance);

                buttonPanel.add(maintenancePanel);

                cpanel.add(buttonPanel, "South");

                frame.pack();
            }

            updateUI(le);
        }
    }

    private void updateUI(Object le) {
        int numBowlers = ((LaneEvent) le).getParty().getMembers().size();
        int[][] lescores = ((LaneEvent) le).getScoreData().getCumulScore();
        for (int k = 0; k < numBowlers; k++) {
                for (int i = 0; i <= ((LaneEvent) le).getFrameInfo().getFrameNum() - 1; i++) {
                    if (lescores[k][i] != 0)
                        scoreLabel[k][i].setText(
                                (new Integer(lescores[k][i])).toString());
                }
                for (int i = 0; i < ConfigFile.getNumFrames(); i++) {
                    int curBowlerScore = ((int[]) ((LaneEvent) le).getScoreData().getScore().get(bowlers.get(k)))[i];
                    int prevScore = 0;
                    if (i > 0)
                        prevScore = ((int[]) ((LaneEvent) le).getScoreData().getScore().get(bowlers.get(k)))[i - 1];

                    if (curBowlerScore!= -1)
                        if (curBowlerScore == 10 && (i % 2 == 0 || i == 19)){
                            ballLabel[k][i].setText("X");
                            emoticons[k].setIcon(new ImageIcon(scaledImg));
                            if(prevScore == 10) {
                                for (int j = 0; j < numBowlers; j++) {
                                    if (k != j) {
                                        int prevScoreForBowler = -1;
                                        if (i > 0)
                                            prevScoreForBowler = ((int[]) ((LaneEvent) le).getScoreData().getScore().get(bowlers.get(j)))[i - 1];
                                        if (prevScoreForBowler != 10)
                                            emoticons[j].setIcon(new ImageIcon(scaledImg2));
                                    }
                                }
                            }
                        }
                        else if (i > 0 && curBowlerScore + prevScore == 10 && i % 2 == 1) {
                            ballLabel[k][i].setText("/");
                            emoticons[k].setIcon(new ImageIcon(scaledImg));
                            if(prevScore == 10) {
                                for (int j = 0; j < numBowlers; j++) {
                                    if(k!=j) {
                                        int prevScoreForBowler = -1;
                                        if (i > 0){
                                            prevScoreForBowler = ((int[]) ((LaneEvent) le).getScoreData().getScore().get(bowlers.get(j)))[i - 1];
                                        }
                                        if(prevScoreForBowler != 10)
                                            emoticons[j].setIcon(new ImageIcon(scaledImg2));
                                    }
                                }
                            }
                        }
                        else if (curBowlerScore == -2)
                            ballLabel[k][i].setText("F");
                        else {
                            ballLabel[k][i].setText(
                                    (new Integer(curBowlerScore).toString()));
                            if(new Integer(curBowlerScore) == 0){
                                emoticons[k].setIcon(new ImageIcon(scaledImg3));
                                //handle gutter ball
                                // int prevScoreForBowler = -1;
                                // if (i > 0 ){
                                //     prevScoreForBowler = ((int[]) ((LaneEvent) le).getScoreData().getScore().get(bowlers.get(j)))[i - 1];
                                //     if (prevScoreForBowler == 0) {

                                //     }
                                // }

                            } else {
                                emoticons[k].setIcon(new ImageIcon(scaledImg4));
                            }
                        }
                }
            }
    }
}
