package views;

import common.Lane;
import common.Pinsetter;
import common.Util;
import models.LaneEvent;
import models.PinsetterEvent;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class LaneStatusView implements Observer {

    private final JPanel jp;
    private final JLabel curBowler;
    //private final JLabel foul;
    private final JLabel pinsDown;
    //private final JButton viewLane;
    private final JButton viewPinSetter;
    private final JButton maintenance;
    private final PinSetterView psv;
    private final LaneView lv;
    int laneNum;
    boolean laneShowing;
    boolean psShowing;

    public LaneStatusView(Lane lane, int laneNum) {

        this.laneNum = laneNum;
        laneShowing = false;
        psShowing = false;
        psv = new PinSetterView(laneNum);
        Pinsetter ps = lane.getPinsetter();
        ps.addObserver(psv);
        lv = new LaneView(lane, laneNum);
        lane.addObserver(lv);

        jp = new JPanel();
        jp.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(5, 5, 5, 5);
        JLabel cLabel = new JLabel("Now Bowling: ");
        curBowler = new JLabel("(no one)");
        JLabel pdLabel = new JLabel("Pins Down: ");
        pinsDown = new JLabel("0");
//        JLabel picLabel = new JLabel(), target = new JLabel();
//        picLabel.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/res/bar.png"))));
//        target.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/res/target.png"))));
//        int startPt = picLabel.getLocation().x;
//        int endPt = startPt + 248;

        /*viewLane = Util.createButton("View Lane");
        viewLane.addActionListener(e -> {
            if (lane.isPartyAssigned()) {
                JFrame frame = lv.getFrame();
                if (!laneShowing) {
                    frame.setVisible(true);
                    // lv.show();
                    laneShowing = true;
                } else {
                    frame.setVisible(false);
                    // lv.hide();
                    laneShowing = false;
                }
            }
        });*/
        viewPinSetter = Util.createButton("View Lane/Pinsetter");
        viewPinSetter.addActionListener(e -> {
            if (lane.isPartyAssigned()) {
                JFrame frame = lv.getFrame();
                if (!psShowing) {
                    psv.show();
                    frame.setVisible(true);
                    psShowing = true;
                } else {
                    psv.hide();
                    frame.setVisible(false);
                    psShowing = false;
                }
            }
        });

//        JButton bowlNow = Util.createButton("Bowl");
//        bowlNow.addActionListener(e -> {
//            System.out.println(Math.abs(target.getLocation().x - (startPt + endPt) / 2));
//        });

        maintenance = Util.createButton("Resume");
        maintenance.addActionListener(e -> {
            if (lane.isPartyAssigned()) {
                lane.unPauseGame();
                maintenance.setBackground(Color.GREEN);
            }
        });
        maintenance.setBackground(Color.GREEN);

        //viewLane.setEnabled(false);
        viewPinSetter.setEnabled(false);

        c.gridx = 0;
        c.gridy = 0;
        jp.add(cLabel, c);
        c.gridx = 1;
        jp.add(curBowler, c);
//		jp.add( fLabel );
//		jp.add( foul );
        c.gridx = 2;
        jp.add(pdLabel, c);
        c.gridx = 3;
        jp.add(pinsDown, c);
        c.gridx = 4;
//        jp.add(viewLane, c);
//        c.gridx = 5;
        jp.add(viewPinSetter, c);
        c.gridx = 6;
        jp.add(maintenance, c);
        /*c.gridx = 0;
        c.gridy = 1;
        jp.add(picLabel, c);
        c.gridx = 1;
        jp.add(bowlNow, c);
        c.gridx = 0;
        target.setLocation(picLabel.getLocation());
        jp.add(target, c);
        int[] dir = {1};
        Timer timer = new Timer(6, e -> {
            int currLoc = target.getLocation().x;
            if (currLoc >= endPt)
                dir[0] = -1;
            else if (currLoc <= startPt)
                dir[0] = 1;
            target.setLocation(target.getLocation().x + dir[0], target.getLocation().y);
        });
        timer.start();*/
    }

    public JPanel showLane() {
        return jp;
    }

    @Override
    public void update(Observable lane, Object event) {
        if (event instanceof LaneEvent) {
            curBowler.setText(((LaneEvent) event).getBowler().getNickName());
            if (((LaneEvent) event).isMechanicalProblem()) {
                maintenance.setBackground(Color.RED);
            }
            //                viewLane.setEnabled(false);
            //                viewLane.setEnabled(true);
            viewPinSetter.setEnabled(((LaneEvent) event).isPartyAssigned);
        } else if (event instanceof PinsetterEvent) {
            pinsDown.setText((new Integer(((PinsetterEvent) event).totalPinsDown())).toString());
        }
    }

//     public void update(PinsetterEvent pe) {
//         pinsDown.setText((new Integer(pe.totalPinsDown())).toString());
// //		foul.setText( ( new Boolean(pe.isFoulCommited()) ).toString() );

//     }
}