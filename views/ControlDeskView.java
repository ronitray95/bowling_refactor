package views;
/* ControlDeskView.java
 *
 *  Version:
 *			$Id$
 *
 *  Revisions:
 * 		$Log$
 *
 */

import common.ControlDesk;
import common.Lane;
import common.Util;
import common.regParty;
import models.ControlDeskEvent;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

/**
 * Class for representation of the control desk
 */
public class ControlDeskView implements Observer {

    private final JFrame win;
    private final JList<String> partyList;

    /**
     * Displays a GUI representation of the ControlDesk
     */
    public ControlDeskView(ControlDesk controlDesk, int maxMembers, int numLanes) {
        // int numLanes = controlDesk.getNumLanes();

        win = new JFrame("Control Desk");
        win.getContentPane().setLayout(new BorderLayout());
        ((JPanel) win.getContentPane()).setOpaque(false);

        JPanel colPanel = new JPanel();
        colPanel.setLayout(new BorderLayout());

        // Controls Panel
        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridLayout(3, 1));
        controlsPanel.setBorder(new TitledBorder("Controls"));

        JButton addParty = Util.createButton("Add Party");
        addParty.addActionListener(e -> new AddPartyView(ControlDeskView.this, maxMembers));
        JPanel addPartyPanel = new JPanel();
        addPartyPanel.setLayout(new FlowLayout());
        //addParty.addActionListener(this);
        addPartyPanel.add(addParty);
        controlsPanel.add(addPartyPanel);

        JButton assign = Util.createButton("Assign Lanes");
        assign.addActionListener(e -> controlDesk.assignLane());
        JPanel assignPanel = new JPanel();
        assignPanel.setLayout(new FlowLayout());
        assignPanel.add(assign);
//		controlsPanel.add(assignPanel);

        JButton scoreBtn = Util.createButton("Queries");
        scoreBtn.addActionListener(e -> new QueryView(ControlDeskView.this));
//        {
//            win.setVisible(false);
//            System.exit(0);
//        });
        JPanel scorePanel = new JPanel();
        scorePanel.setLayout(new FlowLayout());
        scorePanel.add(scoreBtn);
        controlsPanel.add(scorePanel);

        JButton finished = Util.createButton("Finished");
        finished.addActionListener(e -> {
            win.setVisible(false);
            System.exit(0);
        });
        JPanel finishedPanel = new JPanel();
        finishedPanel.setLayout(new FlowLayout());
        finishedPanel.add(finished);
        controlsPanel.add(finishedPanel);

        // Lane Status Panel
        JPanel laneStatusPanel = new JPanel();
        laneStatusPanel.setLayout(new GridLayout(numLanes, 1));
        laneStatusPanel.setBorder(new TitledBorder("Lane Status"));

        HashSet<Lane> lanes = controlDesk.getLanes();
        int laneCount = 0;
        for (Lane curLane : lanes) {
            LaneStatusView laneStat = new LaneStatusView(curLane, (laneCount + 1));
            curLane.addObserver(laneStat);
            curLane.getPinsetter().addObserver(laneStat);
            JPanel lanePanel = laneStat.showLane();
            lanePanel.setBorder(new TitledBorder("Lane" + ++laneCount));
            laneStatusPanel.add(lanePanel);
        }

        // Party Queue Panel
        JPanel partyPanel = new JPanel();
        partyPanel.setLayout(new FlowLayout());
        partyPanel.setBorder(new TitledBorder("Party Queue"));

        Vector<String> empty = new Vector<>();
        empty.add("(Empty)");

        partyList = new JList<>(empty);
        partyList.setFixedCellWidth(120);
        partyList.setVisibleRowCount(10);
        JScrollPane partyPane = new JScrollPane(partyList);
        partyPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        partyPanel.add(partyPane);
        //		partyPanel.add(partyList);

        // Clean up main panel
        colPanel.add(controlsPanel, "East");
        colPanel.add(laneStatusPanel, "Center");
        colPanel.add(partyPanel, "West");

        win.getContentPane().add("Center", colPanel);

        win.pack();

        /* Close program when this window closes */
        win.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // Center Window on Screen
        Dimension screenSize = (Toolkit.getDefaultToolkit()).getScreenSize();
        win.setLocation(((screenSize.width) / 2) - ((win.getSize().width) / 2), ((screenSize.height) / 2) - ((win.getSize().height) / 2));
        win.setVisible(true);

    }

    /*
     * Handler for actionEvents
     *
     * @param e the ActionEvent that triggered the handler

    /*@Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(addParty)) {
            new AddPartyView(this, maxMembers);
            //AddPartyView addPartyWin = new AddPartyView(this, maxMembers);
        }
        if (e.getSource().equals(assign)) {
            controlDesk.assignLane();
        }
        if (e.getSource().equals(finished)) {
            win.setVisible(false);
            System.exit(0);
        }
    }*/

    /**
     * Receive a new party from andPartyView.
     *
     * @param addPartyView the AddPartyView that is providing a new party
     */

    public void updateAddParty(AddPartyView addPartyView) {
        regParty.addPartyQueue(addPartyView.getParty());
    }

    /*
     * Receive a broadcast from a ControlDesk
     *
     * @param ce the ControlDeskEvent that triggered the handler
     * /
    public void receiveControlDeskEvent(ControlDeskEvent ce) {
        partyList.setListData(ce.getPartyQueue());
    }*/

    @Override
    public void update(Observable lane, Object event) {
        partyList.setListData(((ControlDeskEvent) event).getPartyQueue());
    }
}