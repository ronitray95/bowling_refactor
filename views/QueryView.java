package views;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import models.Bowler;

import java.io.BufferedReader;
import java.io.FileReader;
import common.BowlerFile;
import common.ScoreHistoryFile;
import common.ConfigFile;
import common.Util;
import models.Score;

public class QueryView implements ListSelectionListener{

//    private final int maxSize;
//    private final JFrame win;
//    private final JList<String> partyList;
//    // private final JList<String> allBowlers;
//    private final Vector<String> party;
//    private final ControlDeskView controlDesk;
    private final JButton[] patronButtons;
//    //private Integer lock;
//    private Vector<String> bowlerdb;
//    private Vector<String> name;
//    private Vector<Integer> score;
//    private String selectedNick, selectedMember;

    private int maxSize;
    private JFrame win;
    private JList<String> partyList;
    private JList<String> allBowlers;
    private Vector<String> party;
    //private Integer lock;
    private Vector<String> bowlerdb;
    private String selectedNick, selectedMember;

    // private static String BOWLER_DAT = "BOWLERS.DAT";

    /**
     * Constructor for GUI used to Add Parties to the waiting party queue.
     */
    public QueryView(ControlDeskView controlDesk) {

//        this.controlDesk = controlDesk;
        // private static final String BOWLER_DAT = "BOWLERS.DAT";
//        maxSize = max;

        win = new JFrame("Queries");
        win.getContentPane().setLayout(new BorderLayout());
        ((JPanel) win.getContentPane()).setOpaque(false);

        JPanel colPanel = new JPanel();
        colPanel.setLayout(new GridLayout(1, 3));

        // Party Panel
        JPanel partyPanel = new JPanel();
        partyPanel.setLayout(new FlowLayout());
        partyPanel.setBorder(new TitledBorder("Results"));

        party = new Vector<>();
        Vector<String> empty = new Vector<>();
        empty.add("(Empty)");

        partyList = new JList<>(empty);
        partyList.setFixedCellWidth(200);
        partyList.setVisibleRowCount(10);
        partyList.addListSelectionListener(this);
        JScrollPane partyPane = new JScrollPane(partyList);
        partyPanel.add(partyPane);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(3, 1));
        patronButtons = new JButton[6];//addPatron,remPatron,newPatron,finished
        String[] buttonLabels = {"Top players", "Highest Score", "Highest Score of ", "Lowest Score", "Subscribe Date", "Finished"};

//        for (int i = 0; i < 6; i++) {
//            patronButtons[i] = Util.createButton(buttonLabels[i]);
//            JPanel jPanel = new JPanel();
//            jPanel.setLayout(new FlowLayout());
//            jPanel.add(patronButtons[i]);
//            buttonPanel.add(jPanel);
//        }

        for (int i = 0; i < 6; i++) {
            patronButtons[i] = Util.createButton(buttonLabels[i]);
            int finalI = i;
            patronButtons[i].addActionListener(e -> {
                if (finalI == 0) {
                    party.clear();
                    Vector<Score> in = ScoreHistoryFile.getInfo();
                    Score data;
                    Map<Integer, Score> map = new HashMap<>();
                    int l = in.size();
                    for (int j = 0; j < l; j++) {
                        data = in.get(j);

//                    String[] bowler = data.split("\t");
                        map.put(Integer.parseInt(data.getScore()), data);
                    }
                    TreeMap<Integer, Score> sorted = new TreeMap<>(map);
                    Map<Integer, Score> reverseMap = sorted.descendingMap();

                    int j = 0;
                    for (Map.Entry<Integer, Score> entry : reverseMap.entrySet()) {
                        System.out.print(ConfigFile.getTopPlayers());
                        if (j == ConfigFile.getTopPlayers())
                            break;
//                    String[] s=entry.getValue().split("\t");
                        party.add(j + 1 + ". Name: " + entry.getValue().getNickName() + " Score: " + entry.getValue().getScore());
                        j++;
                    }
                    partyList.setListData(party);

                } else if (finalI == 1) {
                    party.clear();
                    Vector<Score> in = ScoreHistoryFile.getInfo();
                    Score data;
                    Map<Integer, Score> map = new HashMap<>();
                    int l = in.size();
                    for (int j = 0; j < l; j++) {
                        data = in.get(j);

//                    String[] bowler = data.split("\t");
                        map.put(Integer.parseInt(data.getScore()), data);
                    }
                    TreeMap<Integer, Score> sorted = new TreeMap<>(map);
                    Map<Integer, Score> reverseMap = sorted.descendingMap();

                    for (Map.Entry<Integer, Score> entry : reverseMap.entrySet()) {
                        party.add("Name: " + entry.getValue().getNickName() + " Score: " + entry.getValue().getScore());
                        break;
                    }
                    partyList.setListData(party);
                } else if (finalI == 2) {
                    party.clear();
                    JFrame jFrame = new JFrame();
                    String getMessage = JOptionPane.showInputDialog(jFrame, "Enter the Nick Name");

                    Vector<Score> in = ScoreHistoryFile.getInfo();
                    Score data, ans = new Score("", "", "");
//                String ans="";
                    int l = in.size();
                    int max = -1;
                    for (int j = 0; j < l; j++) {
                        data = in.get(j);
                        if (getMessage.equals(data.getNickName()) && Integer.parseInt(data.getScore()) >= max) {
                            max = Integer.parseInt(data.getScore());
                            ans = data;
                        }
                    }
                    if (max == -1)
                        JOptionPane.showMessageDialog(jFrame, getMessage + " has no games/Subscription");
                    else
                        JOptionPane.showMessageDialog(jFrame, "Highest Score of " + getMessage + ": " + max + " on: " + ans.getDate());
                } else if (finalI == 3) {
                    party.clear();
                    Vector<Score> in = ScoreHistoryFile.getInfo();
                    Score data;
                    Map<Integer, Score> map = new HashMap<>();
                    int l = in.size();
                    for (int j = 0; j < l; j++) {
                        data = in.get(j);

//                    String[] bowler = data.split("\t");
                        map.put(Integer.parseInt(data.getScore()), data);
                    }
                    TreeMap<Integer, Score> sorted = new TreeMap<>(map);
                    Map<Integer, Score> reverseMap = sorted;

                    for (Map.Entry<Integer, Score> entry : reverseMap.entrySet()) {
                        party.add("Name: " + entry.getValue().getNickName() + " Score: " + entry.getValue().getScore());
                        break;
                    }
                    partyList.setListData(party);
                } else if (finalI == 4) {
                    party.clear();
                    JFrame jFrame = new JFrame();
                    String getMessage = JOptionPane.showInputDialog(jFrame, "Enter the Nick Name");

                    Vector<Score> in = ScoreHistoryFile.getInfo();
                    Score data;
                    String ans = "";
                    int l = in.size();
                    for (int j = 0; j < l; j++) {
                        data = in.get(j);
                        if (getMessage.equals(data.getNickName())) {
                            ans = data.getDate();
                            break;
                        }
                    }
                    if (ans.equals(""))
                        JOptionPane.showMessageDialog(jFrame, getMessage + " has no Subscription");
                    else
                        JOptionPane.showMessageDialog(jFrame, "Subscribed date of " + getMessage + ": " + ans);
                }
                if (finalI == 5) {
                    win.setVisible(false);
                }
            });
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new FlowLayout());
            jPanel.add(patronButtons[i]);
            buttonPanel.add(jPanel);
        }

            colPanel.add(partyPanel);
            colPanel.add(buttonPanel);

            win.getContentPane().add("Center", colPanel);
            win.pack();

            // Center Window on Screen
            Dimension screenSize = (Toolkit.getDefaultToolkit()).getScreenSize();
            win.setLocation(
                    ((screenSize.width) / 2) - ((win.getSize().width) / 2),
                    ((screenSize.height) / 2) - ((win.getSize().height) / 2));
            win.setVisible(true);

        }

//    @Override
//    public void actionPerformed(ActionEvent e) {
//        if (e.getSource().equals(patronButtons[0])) {
//            party.clear();
//            try{
//                Vector<Score> in = ScoreHistoryFile.getInfo();
//                Score data;
//                Map<Integer, Score> map = new HashMap<>();
//                int l = in.size();
//                for(int i=0;i<l;i++) {
//                    data = in.get(i);
//
////                    String[] bowler = data.split("\t");
//                    map.put( Integer.parseInt(data.getScore()),data);
//                }
//                TreeMap< Integer, Score> sorted = new TreeMap<>(map);
//                Map<Integer, Score> reverseMap= sorted.descendingMap();
//
//                int i=0;
//                for (Map.Entry< Integer,Score> entry : reverseMap.entrySet()) {
//                    if(i==ConfigFile.getTopPlayers())
//                        break;
////                    String[] s=entry.getValue().split("\t");
//                    party.add(i+1+". Name: "+entry.getValue().getNickName()+" Score: "+entry.getValue().getScore());
//                    i++;
//                }
//                partyList.setListData(party);
//            }catch(Exception ex){
//                System.err.println("File Error");
//            }
//        }
//        if (e.getSource().equals(patronButtons[1])) {
//            party.clear();
//            try{
//                Vector<Score> in = ScoreHistoryFile.getInfo();
//                Score data;
//                Map<Integer, Score> map = new HashMap<>();
//                int l = in.size();
//                for(int i=0;i<l;i++) {
//                    data = in.get(i);
//
////                    String[] bowler = data.split("\t");
//                    map.put( Integer.parseInt(data.getScore()),data);
//                }
//                TreeMap< Integer, Score> sorted = new TreeMap<>(map);
//                Map<Integer, Score> reverseMap= sorted.descendingMap();
//
//                int i=0;
//                for (Map.Entry< Integer,Score> entry : reverseMap.entrySet()) {
//                    party.add("Name: "+entry.getValue().getNickName()+" Score: "+entry.getValue().getScore());
//                    break;
//                }
//                partyList.setListData(party);
//            }catch(Exception ex){
//                System.err.println("File Error");
//            }
//        }
//
//        if (e.getSource().equals(patronButtons[2])) {
//            party.clear();
//            try{
//                JFrame jFrame = new JFrame();
//                String getMessage = JOptionPane.showInputDialog(jFrame, "Enter the Nick Name");
//
//                Vector<Score> in = ScoreHistoryFile.getInfo();
//                Score data,ans=new Score("","","");
////                String ans="";
//                int l = in.size();
//                int max=-1;
//                for(int i=0;i<l;i++) {
//                    data = in.get(i);
//                    if(getMessage.equals(data.getNickName()) && Integer.parseInt(data.getScore())>=max){
//                        max = Integer.parseInt(data.getScore());
//                        ans = data;
//                    }
//                }
//                if(max==-1)
//                    JOptionPane.showMessageDialog(jFrame, getMessage+" has no games/Subscription");
//                else
//                    JOptionPane.showMessageDialog(jFrame, "Highest Score of "+getMessage+": "+max+" on: "+ans.getDate());
//            }catch(Exception ex){
//                System.err.println("File Error");
//            }
//        }
//
//        if (e.getSource().equals(patronButtons[3])) {
//            party.clear();
//            try{
//                Vector<Score> in = ScoreHistoryFile.getInfo();
//                Score data;
//                Map<Integer, Score> map = new HashMap<>();
//                int l = in.size();
//                for(int i=0;i<l;i++) {
//                    data = in.get(i);
//
////                    String[] bowler = data.split("\t");
//                    map.put( Integer.parseInt(data.getScore()),data);
//                }
//                TreeMap< Integer, Score> sorted = new TreeMap<>(map);
//                Map<Integer, Score> reverseMap= sorted;
//
//                int i=0;
//                for (Map.Entry< Integer,Score> entry : reverseMap.entrySet()) {
//                    party.add("Name: "+entry.getValue().getNickName()+" Score: "+entry.getValue().getScore());
//                    break;
//                }
//                partyList.setListData(party);
//            }catch(Exception ex){
//                System.err.println("File Error");
//            }
//        }
//        if (e.getSource().equals(patronButtons[4])) {
//            party.clear();
//            try{
//                JFrame jFrame = new JFrame();
//                String getMessage = JOptionPane.showInputDialog(jFrame, "Enter the Nick Name");
//
//                Vector<Score> in = ScoreHistoryFile.getInfo();
//                Score data;
//                String ans="";
//                int l = in.size();
//                for(int i=0;i<l;i++) {
//                    data = in.get(i);
//                    if(getMessage.equals(data.getNickName())){
//                        ans = data.getDate();
//                        break;
//                    }
//                }
//                if(ans.equals(""))
//                    JOptionPane.showMessageDialog(jFrame, getMessage+" has no Subscription");
//                else
//                    JOptionPane.showMessageDialog(jFrame, "Subscribed date of "+getMessage+": "+ans);
//            }catch(Exception ex){
//                System.err.println("File Error");
//            }
//        }
//        if (e.getSource().equals(patronButtons[4])) {
//            win.setVisible(false);
//        }
//
//    }

    /**
     * Handler for List actions
     *
     * @param e the ListActionEvent that triggered the handler
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource().equals(patronButtons[0])) {
            selectedNick =
                    ((String) ((JList<?>) e.getSource()).getSelectedValue());
        }
    }

    /**
     * Accessor for Party
     * <p>
     * <p>
     * public Vector<String> getNames() {
     * return party;
     * }/*
     * <p>
     * /**
     * Called by NewPatronView to notify AddPartyView to update
     *
     * @param newPatron the NewPatronView that called this method
     */
    // public void updateNewPatron(NewPatronView newPatron) {
    //     // try {
    //     //     Bowler checkBowler = BowlerFile.getBowlerInfo(newPatron.getNick());
    //     //     if (checkBowler == null) {
    //     //         BowlerFile.putBowlerInfo(
    //     //                 newPatron.getNick(),
    //     //                 newPatron.getFull(),
    //     //                 newPatron.getEmail());
    //     //         bowlerdb = new Vector<>(BowlerFile.getBowlers());
    //     //         // allBowlers.setListData(bowlerdb);
    //     //         party.add(newPatron.getNick());
    //     //         partyList.setListData(party);
    //     //     } else {
    //     //         System.err.println("A Bowler with that name already exists.");
    //     //     }
    //     // } catch (Exception e2) {
    //     //     System.err.println("File I/O Error");
    //     // }
    //     try{
    //         BufferedReader in = ScoreHistoryFile.getIn();
    //         String data;
    //         Map<Integer, String> map = new HashMap<>();
    //         while ((data = in.readLine()) != null) {
    //             String[] bowler = data.split("\t");
    //             // System.out.println(bowler[0]+ bowler[2]);
    //             map.put( Integer.parseInt(bowler[2]),data);
    //             // System.out.println(map);
    //         }
    //         TreeMap< Integer, String> sorted = new TreeMap<>(map);
    //         Map<Integer, String> reverseMap= sorted.descendingMap();
    //         // SortedSet<Integer> sorted = new TreeSet<>(map.values());
    //         Vector<String> op= new Vector<>();
    //         int i=0;
    //         for (Map.Entry< Integer,String> entry : reverseMap.entrySet()) {
    //             if(i==10)
    //                 break;
    //             String[] s=entry.getValue().split("\t");
    //             party.add("Name: "+s[0]+" Score: "+s[2]);
    //             // System.out.println(op.get(i));
    //             i++;
    //         }
    //         partyList.setListData(party);
    //     }catch(Exception ex){
    //         System.err.println("File Error");
    //     }
    // }

    // /**
    //  * Accessor for Party
    //  */
    // public Vector<String> getParty() {
    //     return party;
    // }

}

