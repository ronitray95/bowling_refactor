package views;

/* AddPartyView.java
 *
 *  Version
 *  $Id$
 *
 *  Revisions:
 * 		$Log: NewPatronView.java,v $
 * 		Revision 1.3  2003/02/02 16:29:52  ???
 * 		Added ControlDeskEvent and ControlDeskObserver. Updated Queue to allow access to Vector so that contents could be viewed without destroying. Implemented observer model for most of ControlDesk.
 *
 *
 */

import common.Util;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Class for GUI components need to add a patron
 */
public class NewPatronView {

    //private int maxSize;

    private final JFrame win;
    private final JTextField nickField;
    private final JTextField fullField;
    private final JTextField emailField;
    //private String selectedNick, selectedMember;
    private final AddPartyView addParty;

    //private boolean done;
    private String nick, full, email;

    public NewPatronView(AddPartyView v) {

        addParty = v;
        //done = false;

        win = new JFrame("Add Patron");
        win.getContentPane().setLayout(new BorderLayout());
        ((JPanel) win.getContentPane()).setOpaque(false);

        JPanel colPanel = new JPanel();
        colPanel.setLayout(new BorderLayout());

        // Patron Panel
        JPanel patronPanel = new JPanel();
        patronPanel.setLayout(new GridLayout(3, 1));
        patronPanel.setBorder(new TitledBorder("Your Info"));

        JPanel nickPanel = new JPanel();
        nickPanel.setLayout(new FlowLayout());
        JLabel nickLabel = new JLabel("Nick Name");
        nickField = new JTextField("", 15);
        nickPanel.add(nickLabel);
        nickPanel.add(nickField);

        JPanel fullPanel = new JPanel();
        fullPanel.setLayout(new FlowLayout());
        JLabel fullLabel = new JLabel("Full Name");
        fullField = new JTextField("", 15);
        fullPanel.add(fullLabel);
        fullPanel.add(fullField);

        JPanel emailPanel = new JPanel();
        emailPanel.setLayout(new FlowLayout());
        JLabel emailLabel = new JLabel("E-Mail");
        emailField = new JTextField("", 15);
        emailPanel.add(emailLabel);
        emailPanel.add(emailField);

        patronPanel.add(nickPanel);
        patronPanel.add(fullPanel);
        patronPanel.add(emailPanel);

        // Button Panel
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(4, 1));

        JButton finished = Util.createButton("Add Patron");
        finished.addActionListener(e -> {
            nick = nickField.getText();
            full = fullField.getText();
            email = emailField.getText();
            //done = true;
            addParty.updateNewPatron(NewPatronView.this);
            win.setVisible(false);
        });
        JPanel finishedPanel = new JPanel();
        finishedPanel.setLayout(new FlowLayout());
        finishedPanel.add(finished);

        JButton abort = Util.createButton("Abort");
        abort.addActionListener(e -> win.setVisible(false));
        JPanel abortPanel = new JPanel();
        abortPanel.setLayout(new FlowLayout());
        abortPanel.add(abort);

        buttonPanel.add(abortPanel);
        buttonPanel.add(finishedPanel);

        // Clean up main panel
        colPanel.add(patronPanel, "Center");
        colPanel.add(buttonPanel, "East");

        win.getContentPane().add("Center", colPanel);

        win.pack();

        // Center Window on Screen
        Dimension screenSize = (Toolkit.getDefaultToolkit()).getScreenSize();
        win.setLocation(
                ((screenSize.width) / 2) - ((win.getSize().width) / 2),
                ((screenSize.height) / 2) - ((win.getSize().height) / 2));
        win.setVisible(true);

    }

    public String getNick() {
        return nick;
    }

    public String getFull() {
        return full;
    }

    public String getEmail() {
        return email;
    }

}