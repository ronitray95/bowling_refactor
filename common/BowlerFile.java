package common;
/* BowlerFile.java
 *
 *  Version:
 *  		$Id$
 *
 *  Revisions:
 * 		$Log: BowlerFile.java,v $
 * 		Revision 1.5  2003/02/02 17:36:45  ???
 * 		Updated comments to match javadoc format.
 *
 * 		Revision 1.4  2003/02/02 16:29:52  ???
 * 		Added ControlDeskEvent and ControlDeskObserver. Updated Queue to allow access to Vector so that contents could be viewed without destroying. Implemented observer model for most of ControlDesk.
 *
 *
 */

import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.InsertOneResult;
import models.Bowler;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.Vector;

/**
 * Class for interfacing with Bowler database
 */
public class BowlerFile {

    /*
     * The location of the bowelr database
     */
//    private static final String BOWLER_DAT = "BOWLERS.DAT";

    /**
     * Retrieves bowler information from the database and returns a Bowler objects with populated fields.
     *
     * @param nickName the nickName of the bolwer to retrieve
     * @return a Bowler object
     */
    public static Bowler getBowlerInfo(String nickName) {
        MongoCollection<Document> collection = Database.getCollection("bowlers");
        Bowler b = null;
        Bson projectionFields = Projections.fields(
                Projections.include("nick", "name", "email"),
                Projections.excludeId());
        try (MongoCursor<Document> cursor = collection.find(Filters.eq("nick", nickName))
                .projection(projectionFields)
                .iterator()) {
            if (cursor.hasNext()) {
                Document gg = cursor.next();
                b = new Bowler((String) gg.get("nick"), (String) gg.get("name"), (String) gg.get("email"));
            }
        }
        System.out.println("Nick not found...");
        return b;
    }

    /**
     * Stores a Bowler in the database
     *
     * @param nickName the NickName of the Bowler
     * @param fullName the FullName of the Bowler
     * @param email    the E-mail Address of the Bowler
     */
    public static void putBowlerInfo(String nickName, String fullName, String email) {
        MongoCollection<Document> collection = Database.getCollection("bowlers");
        try {
            InsertOneResult result = collection.insertOne(new Document()
                    .append("_id", new ObjectId())
                    .append("nick", nickName)
                    .append("name", fullName)
                    .append("email", email));
            System.out.println("Success! Inserted document id: " + result.getInsertedId());
        } catch (MongoException me) {
            System.err.println("Unable to insert due to an error: " + me);
        }
    }

    /**
     * Retrieves a list of nicknames in the bowler database
     *
     * @return a Vector of Strings
     */

    public static Vector<String> getBowlers() {
        Vector<String> allBowlers = new Vector<>();
        MongoCollection<Document> collection = Database.getCollection("bowlers");
        Bson projectionFields = Projections.fields(
                Projections.include("nick"),
                Projections.excludeId());
        try (MongoCursor<Document> cursor = collection.find()
                .projection(projectionFields)
                .iterator()) {
            while (cursor.hasNext()) {
                String x = String.valueOf(cursor.next().get("nick"));
                if (!x.equalsIgnoreCase("null"))
                    allBowlers.add(x);
            }
        }
        System.out.println(allBowlers);
        return allBowlers;
    }
}