package common;

import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.InsertOneResult;
import models.LaneEvent;
import models.Score;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

public class ScoreHistoryFile implements Observer {

    //private static final String SCOREHISTORY_DAT = "SCOREHISTORY.DAT";

    public static void addScore(String nick, String score) {
        MongoCollection<Document> collection = Database.getCollection("score_history");
        String date = Util.getCurrentDateString();
        try {
            InsertOneResult result = collection.insertOne(new Document()
                    .append("_id", new ObjectId())
                    .append("nick", nick)
                    .append("date", date)
                    .append("score", score));
            System.out.println("Success! Inserted document id: " + result.getInsertedId());
        } catch (MongoException me) {
            System.err.println("Unable to insert due to an error: " + me);
        }
//        String data = nick + "\t" + date + "\t" + score + "\n";
//        RandomAccessFile out = new RandomAccessFile(SCOREHISTORY_DAT, "rw");
//        out.skipBytes((int) out.length());
//        out.writeBytes(data);
//        out.close();
    }

    public static Vector<Score> getInfo() {
        MongoCollection<Document> collection = Database.getCollection("score_history");
        Vector<Score> s = new Vector<>();

        Bson projectionFields = Projections.fields(
                Projections.include("nick", "date", "score"),
                Projections.excludeId());
        try (MongoCursor<Document> cursor = collection.find()
                .projection(projectionFields)
                .iterator()) {
            while (cursor.hasNext()) {
//                System.out.println(cursor.next().toJson());
                Document gg = cursor.next();
                s.add(new Score((String) gg.get("nick"), (String) gg.get("date"), (String) gg.get("score")));
            }
        }
        return s;
    }

    public static Vector<Score> getScores(String nick) {
        MongoCollection<Document> collection = Database.getCollection("score_history");
        Vector<Score> s = new Vector<>();

        Bson projectionFields = Projections.fields(
                Projections.include("nick", "date", "score"),
                Projections.excludeId());
        try (MongoCursor<Document> cursor = collection.find(Filters.eq("nick", nick))
                .projection(projectionFields)
                .iterator()) {
            if (cursor.hasNext()) {
//                System.out.println(cursor.next().toJson());
                Document gg = cursor.next();
                s.add(new Score((String) gg.get("nick"), (String) gg.get("date"), (String) gg.get("score")));
            }
        }
        return s;
        /*Vector<Score> scores = new Vector<>();
        BufferedReader in = new BufferedReader(new FileReader(SCOREHISTORY_DAT));
        String data;
        while ((data = in.readLine()) != null) {
            // File format is nick\tfname\te-mail
            String[] scoredata = data.split("\t");
            //"Nick: scoredata[0] Date: scoredata[1] Score: scoredata[2]
            if (nick.equals(scoredata[0]))
                scores.add(new Score(scoredata[0], scoredata[1], scoredata[2]));
        }
        return scores;*/
    }

    @Override
    public void update(Observable planeObservable, Object laneEvent) {
        LaneEvent ev = (LaneEvent) laneEvent;
        try {
            addScore(ev.getBowler().getNickName(), Integer.toString(ev.getScoreData().getCumulScore()[ev.getFrameInfo().getIndex()][9]));
            // ScoreHistoryFile.addScore(currentThrower.getNickName(), Integer.toString(cumulScores[bowlIndex][9]));
        } catch (Exception e) {
            System.err.println("Exception in addScore. " + e);
        }
    }
}