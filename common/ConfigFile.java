package common;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ConfigFile {
    private static final String CONFIG = "CONFIG.DAT";

    private static int numLanes;
    private static int maxPatronsPerParty;
    private static int numFrames;
    private static int topPlayers;
    private static int numPins;

    public static void processData() throws IOException{
        BufferedReader in = new BufferedReader(new FileReader(CONFIG));
        String data;
        int i=0;
        while ((data = in.readLine()) != null) {
            // File format is numLanes\maxPatronsPerParty\numFrames\topPlayers\numPins
            if(i==1){
                String[] configdata = data.split(" ");
                //"Nick: scoredata[0] Date: scoredata[1] Score: scoredata[2]
                // System.out.println(configdata[0]+ configdata[1]+configdata[2]+configdata[4]);
                numLanes = Integer.valueOf(configdata[0]);
                numLanes = Integer.parseInt(configdata[0]);
                maxPatronsPerParty = Integer.parseInt(configdata[1]);
                numFrames = Integer.parseInt(configdata[2]);
                topPlayers= Integer.parseInt(configdata[3]);
                numPins = Integer.parseInt(configdata[4]);
            }
            i++;
        }
    }

    public static int getNumLanes(){
        return numLanes;
    }

    public static int getMaxPatronsPerParty(){
        return maxPatronsPerParty;
    }

    public static int getNumFrames(){
        return numFrames;
    }

    public static int getTopPlayers(){
//        System.out.print(topPlayers);
        return topPlayers;
    }

    public static int getNumPins(){
        return numPins;
    }

}

