package common;

import models.Bowler;
import models.ControlDeskEvent;
import models.Party;

import java.util.Vector;

public class regParty {
    private final static Queue partyQueue = new Queue();
    private final static Queue partyQueue2 = new Queue();

    private static Bowler registerPatron(String nickName) {
        Bowler patron;
//        try {
        // only one patron / nick.... no dupes, no checks
        patron = BowlerFile.getBowlerInfo(nickName);
//        } catch (IOException e) {
//            System.err.println("Error..." + e);
//        }
        return patron;
    }

    public static void addPartyQueue(Vector<String> partyNicks) {
        Vector<Bowler> partyBowlers = new Vector<>();
        for (String partyNick : partyNicks) {
            Bowler newBowler = registerPatron(partyNick);
            partyBowlers.add(newBowler);
        }
        Party newParty = new Party(partyBowlers);
        partyQueue.add(newParty);
        ControlDesk.publish(new ControlDeskEvent(getPartyQueue()));
    }

    public static Vector<String> getPartyQueue() {
        Vector<String> displayPartyQueue = new Vector<>();
        for (int i = 0; i < partyQueue.asVector().size(); i++) {
            String nextParty = ((Party) partyQueue.asVector().get(i)).getMembers()
                    .get(0)
                    .getNickName() + "'s Party";
            displayPartyQueue.addElement(nextParty);
        }
        return displayPartyQueue;
    }

    public static Queue fetchPartyQueue() {
        return partyQueue;
    }



    public static void addPartyQueue2(Vector<String> partyNicks) {
        Vector<Bowler> partyBowlers = new Vector<>();
        for (String partyNick : partyNicks) {
            Bowler newBowler = registerPatron(partyNick);
            partyBowlers.add(newBowler);
        }
        Party newParty = new Party(partyBowlers);
        partyQueue2.add(newParty);
        ControlDesk.publish(new ControlDeskEvent(getPartyQueue2()));
    }

    public static Vector<String> getPartyQueue2() {
        Vector<String> displayPartyQueue = new Vector<>();
        for (int i = 0; i < partyQueue2.asVector().size(); i++) {
            String nextParty = ((Party) partyQueue2.asVector().get(i)).getMembers()
                    .get(0)
                    .getNickName() + "'s Party";
            displayPartyQueue.addElement(nextParty);
        }
        return displayPartyQueue;
    }

    public static Queue fetchPartyQueue2() {
        return partyQueue2;
    }
}