import common.Alley;
import common.ControlDesk;
import views.ControlDeskView;
import common.ConfigFile;

import java.io.IOException;

public class drive {

    public static void main(String[] args) throws IOException {

        ConfigFile.processData();
        int numLanes = ConfigFile.getNumLanes();
        int maxPatronsPerParty = ConfigFile.getMaxPatronsPerParty();

        Alley a = new Alley(numLanes);
        ControlDesk controlDesk = a.getControlDesk();

        ControlDeskView cdv = new ControlDeskView(controlDesk, maxPatronsPerParty, numLanes);
        controlDesk.addObserver(cdv);

    }
}